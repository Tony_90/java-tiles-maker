import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

  public static void main(String[] args) throws IOException {
    Path currentRelativePath = Paths.get("");
    String absolutePath = currentRelativePath.toAbsolutePath().toString();
    new ImageSplitter(absolutePath+"\\neverwinter\\neverwinter.jpg", absolutePath+"\\neverwinter\\tiles\\").split();
    new ImageSplitter(absolutePath+"\\phandalin\\phandalin.jpg", absolutePath+"\\phandalin\\tiles\\").split();
    new ImageSplitter(absolutePath+"\\world\\world.jpg", absolutePath+"\\world\\tiles\\").split();
  }
}
