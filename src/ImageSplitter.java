import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


class ImageSplitter {

  private final String pathFrom;
  private final String pathTo;

  ImageSplitter(String pathFrom, String pathTo) {
    this.pathFrom = pathFrom;
    this.pathTo = pathTo;
  }


  void split() throws IOException {
    BufferedImage originalImage = ImageIO.read(new File(pathFrom));
    for (int z = 0; z < 5; z++)
      this.split(originalImage, z);
  }

  private void split(BufferedImage originalImage, int zoom) throws IOException {
    System.out.println("Zoom: "+ zoom);
    if(checkIfDirectoryAlreadyExist(zoom)){
      System.out.println("Skipping zoom " + zoom + " because already exists the folder...");
      return;
    }

    int height = 256;
    int width = 256;
    int i = 0;
    int j = 0;
    int limit = (int) Math.pow(2, zoom);
    for (i = 0; i < limit; i++) {
      for (j = 0; j < limit; j++) {
        BufferedImage resizedImage = new BufferedImage(250, 250, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage.getSubimage(i * originalImage.getWidth() / limit, j * originalImage.getHeight() / limit, originalImage.getWidth() / limit, originalImage.getHeight() / limit), 0, 0, width, height, null);
        String directoryPath = pathTo + zoom;
        File directory = new File(String.valueOf(directoryPath));
        if (!directory.exists()) directory.mkdir();
        ImageIO.write(resizedImage, "PNG", new File(directoryPath + "\\" + "x" + i + "-" + "y" + j + ".png"));
        g.dispose();
      }
    }
  }

  private boolean checkIfDirectoryAlreadyExist(int zoom){
    String directoryPath = pathTo + zoom;
    File directory = new File(String.valueOf(directoryPath));
    return !directory.exists();
  }

}
